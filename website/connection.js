var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "coffe_shop"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

function getAllData() {
	return con.connect(function(err) {
	  con.query("select a.name as cashier, b.name as product, c.name as category, b.price from cashier a, product b, category c where b.id_category = c.id and b.id_cashier = a.id", function (err, result, fields) {
	    if (err) throw err;
	    console.log(result);
	  });
	});
}

module.export = {
	getAllData: getAllData
}