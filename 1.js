function getProfile() {
  return {
    name: 'Alghifari Fikri Santoso',
    age: 21,
    address: 'Jl. Ciawigebang - Jalaksana RT 06/02 Desa Mekarsari, Kec. Cipicung, Kab. Kuningan - Jabar',
    hobbies: ['Gaming', 'Reading'],
    is_married: false,
    list_school: [{
      name: 'SDN 1 Cimaranten',
      year_in: '2003',
      year_out: '2009',
      major: null
    }, {
      name: 'SMPN 1 Cipicung',
      year_in: '2009',
      year_out: '2012',
      major: null
    },{
      name: 'SMAN 1 Ciawigebang',
      year_in: '2012',
      year_out: '2015',
      major: 'IPA'
    },{
      name: 'Universitas Gunadarma',
      year_in: '2015',
      year_out: '2019',
      major: 'Teknik Informatika'
    }],
    skills: [{
      skill_name: 'Android Developer',
      level: 'advanced'
    },{
      skill_name: 'Oracle SQL Developer',
      level: 'Beginner'
    }] ,
    interest_in_coding: true
  }
}

let profile = getProfile();

let skills = profile.skills;

let list_school = profile.list_school.name;

console.log(JSON.stringify(profile));