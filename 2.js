function is_username_valid(username) {
  
  var lowercaseRe = /[a-z]/g;
  
	if(username.length >= 3 && username.length <=5 && username.match(lowercaseRe).length == username.length) {
    	return true;
    }
    return false;
}

function is_password_valid(password) {
  
  var uppercaseRe = /[A-Z]/g;

  var lowerCaseRe = /[a-z]/g;

  var numberRe = /[0-9]/g;

  var specialRe = /[-]/g;

  if(password.length >= 8 && password.match(numberRe).length == 6 && specialRe.test(password) && (password.match(uppercaseRe) && !password.match(lowerCaseRe))){
     // && numberRe.test(password).length == 6
     // && uppercaseRe.test(password)) {

     return true;
  }

  return false;
}

console.log(is_username_valid('tania') ? 'benar' : 'salah'); //kuduna benar
console.log(is_username_valid('eka') ? 'benar' : 'salah'); //kuduna benar
console.log(is_password_valid('021-333BUDI') ? 'benar' : 'salah'); //kuduna benar
console.log(is_password_valid('021*444DEA') ? 'benar' : 'salah'); //kuduna salah
console.log(is_password_valid('987-654Oliv') ? 'benar' : 'salah'); //kuduna salah

